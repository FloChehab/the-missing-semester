Python
======

*From [wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language))*:
> Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. Its language constructs and object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.
> Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, including procedural, object-oriented, and functional programming. Python is often described as a "batteries included" language due to its comprehensive standard library.


## Things to know regarding python

The standard library in python is indeed incredibly rich? You have set, defaultdict, Counter, lru_cache, etc.
The std lib (check it out) contains tools for the most common algorithmic problems.

There exist also tons of open source packages available on [pypi](https://pypi.org/): you can do pretty much anything with python.

However, Python is *by design* very slow. So python might not be an ideal choice in performance constrained environments. To partially counter this drawback, you will often see packages written in C that have an API in python (numpy, pandas, etc.).

----

If there is one fact that I am willing to tell you about python is that (python) `int`s have an infinite precision. There is no such thing as an `int32` or an `int64`.


## In the real world

* As with any programming language, do your best to use the standard library and get familiar with it.

* When programming inside a team, there will always be never-ending debates about the 'style' of the code. In python you will often see debates around using `"` or `'` for strings, etc.

  However there seem to be the following consensus: in python, we indent the code using spaces and not tabs. Also, your file should be in UTF-8 (applicable to all programming language, encoding is still often a nightmare in engineering projects).

  For the rest, I **STRONGLY** recommend using the code formatter [`black`](https://pypi.org/project/black/), no questions asked.


* Dict are very useful in python (just like Javascript `Object`). They are used a lot. This is not always a great programming habits (they are not immutable and a often data 'bazar'). I strongly recommend using the new `dataclasse`s available since python 3.7.


## Personal notes

Things I don't like about python:

* It's slow,
* The std is not very functional, which makes things look a bit nasty sometimes.
* Duck typing.
* It's not statically typed.

Python was created as a *scripting language*. It's now used for whatever things you can think of, which is not always ideal.


## What I would expect from a python project

* Fully type annotated with `typing`s,
* Formatted with black,
* No complex one-liner.
