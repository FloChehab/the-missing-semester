Javascript
==========

*From [Mozilla Developer portal](https://developer.mozilla.org/en-US/docs/Web/JavaScript):*

> JavaScript (JS) is a lightweight, interpreted, or just-in-time compiled programming language with first-class functions. While it is most well known as the scripting language for Web pages, many non-browser environments also use it, such as Node.js, Apache CouchDB and Adobe Acrobat. JavaScript is a prototype-based, multi-paradigm, single-threaded, dynamic language, supporting object-oriented, imperative, and declarative (e.g. functional programming) styles.


## Essential summary

JavaScript is well known for several things:

* It has nothing to do with `Java`,
* It is rumoured that the very first version of the language was created under 10 days,
* It is what *powers* the modern dynamic frontend experience nowadays (and some backends),
* It is one of the most used language in the world,
* The language itself is standardized and known also known as `ECMAScript`,
* It has the most prolific open source community in the world,
* The language evolves a lot (approximately once every year in the recent time),
* But the language itself maintain an exceptionally strict retro-compatibility principle: old features and ways of doing things are not removed from the language itself.
* Because of this, the language is kind of *YOLO*, and **it definitely has a bunch of haters**.

![troll-js](https://i.redd.it/h7nt4keyd7oy.jpg)


## The « go to » documentation

The best documentation, tutorials, etc. are maintained by the [Mozilla Foundation](https://developer.mozilla.org/en-US/docs/Web/JavaScript), it the first site you should check when you have questions about the language itself.


### Other resources

* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide
* https://github.com/getify/Functional-Light-JS
* https://github.com/getify/You-Dont-Know-JS
* https://javascript.info/

## Technical observations regarding the language itself

Javascript is a strictly **mono-threaded language**. However, it can outperform other competitors because it makes the best use possible of CPU times: it is **asynchronous by design**.


Javascript was initially a language with `functions` and `objects` only. It is now *kind of* object oriented also thanks to the new `class` capabilities. But don't go crazy, the `class` keyword is only a [*syntactic sugar*](https://en.wikipedia.org/wiki/Syntactic_sugar).


Still, Javascript standard library and API is pretty great.


Something important is that the language is **Dynamically weakly typed** which means:
* You don't need to be a specific about the type of a variable before assigning it to a value,
* You can reassign a variable to another data type.


Finally, it is universally recognized that `ECMAScript 6` (or `Javascript 2015`) contributed to the popularity of the language with a cleaner, saner,
easier version of the language.


## Ecosystem

The JavaScript ecosystem is extremely rich with famous frameworks such as React, Angular, Express, etc. This ecosystem is supported by the big tech companies and organization: Google (V8), Facebook (React), LinkedIn, Netflix, Mozilla, etc.

Most of the packages are hosted on `npm` (recently bought by Github (owned by Microsoft)).


### Tools

In this Javascript ecosystem, some packages hold a specific place as they are tools for developers.

**Compilers/Transpilers** (e.g. [`Babel`]()): those tools enable us to use the latest ECMAScript features by basically converting our code to older JavaScript.

**Bundlers** (e.g. [`Webpack`](https://webpack.js.org/)): the easiest way of delivering JavaScript on a web page is to put everything in a single file (the file gets downloaded and parsed sequentially by the browser). However, no developer puts all his/her code in a single file, obviously. So bundlers will read all the file in your project, put everything in a single file (including the project dependencies), so that you don't have to do it yourself (they will also often perform *tree-shaking* to exclude dead/unreachable code from the final file).

**Mimifiers** (e.g. [`minify`](https://www.minifier.org/)): the objective of those tools is to reduce the size of the result of the bundler as much as possible, to:
* Reduce the download time,
* Obfuscate the code (make it machine readable *only*).

Minify and obfuscated Js code may look like this (extract from `google.com`):
```js
ction(a,b){b=void 0===b?{}:b;var c=void 0===b.we?{}:b.we,d=void 0===b.Jx?0:b.Jx;try{s_aaa(s_aa(s_baa),function(e){return e.log(a,c,d)})}catch(e){}},s_caa=function(a){return new RegExp("%(?:"+encodeURIComponent(a).substr(1).replace(/%/g,"|")+")","g")},s_daa=function(){try{if(!s_ca.isEnabled())return!1;s_ca.set("TESTCOOKIESENABLED","1",{hZ:60});if("1"!=s_ca.get("TESTCOOKIESENABLED"))return!1;s_ca.remove("TESTCOOKIESENABLED");return!0}catch(a){return!1}},s_faa=function(a,b,c){s_eaa(a,
b,c)},s_iaa=function(a,b){var c=s_gaa(a),d=function(e){c.set("i",new s_haa({priority:"*",XE:Number.MAX_SAFE_INTEGER},e))};return function(){s_eaa=b;var e=c.get("i");null===e&&d(0);var f=0;null!=e&&(f=e.getValue());e=f;d(e+1);s_eaa=s_da;return e}},s_gaa=function(a){a in s_jaa||(s_jaa[a]=s_kaa("_c",a,s_faa,!1));return s_jaa[a]},s_kaa=fu
```


## Things to be careful about

* The support of the language is not homogenous across all browsers and features: http://kangax.github.io/compat-table/es6/ .


## My opinion

Javascript is a pretty nice language to develop with. But it's full of tricky behaviours which will force you to adopt good coding practices (even applicable to other languages). ***Don't miss this Github repo: https://github.com/denysdovhan/wtfjs** . There also exist other stricter languages (typescript, scala, kotlin, etc.) that can transpile to Js; but I am not familiar with those languages used in such context.

One thing to note in particular is, because you need a complex tool chain for any serious Javascript project, it's a nightmare to start from scratch. But don't get scared, there exist tones of boilerplates on Github.

If you'd like some piece of advice:
* Avoid OOP as much as possible, stay functional,
* Use arrow functions,
* Have fun.
