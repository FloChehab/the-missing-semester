More `git`
==========

## Other important resources

* The [Pro git book](https://git-scm.com/book/en/v2)

* This one is more related to software engineering in general, but you should really get familiar with [semantic versioning](https://semver.org/) and why everyone should follow it.


## Culture

* Why was `git` invented for?
* What are some of its competitors?
* What is a PR (Pull request)? What is a MR (Merge request)?
