git
===

To clarify things a bit this chapter is split in 3 subsections:

* [Working with `git` locally](./git-locally.md)
* [`git` as collaboration tool](./git-collab.md)
* [More `git`](./git-more.md)
