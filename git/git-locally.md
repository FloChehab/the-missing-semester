Working with `git` locally
==========================

## Intro

The important thing with `git` is to understand how it works (not necessarily in minute details) before using it.

A very good resource to start with is [this one](https://missing.csail.mit.edu/2020/version-control/) (I haven't checked the associated video, but if you are not familiar or confident with using `git` you might want to have a look at it).

Some of the fundamentals include:

* What is a commit?
* What is the data structure of your git repository?
* What is a blob?
* What is the data structure of your history?


Make sure you understand what is the *staging area* in `git` vs the *current workdir* and what it's used for.

Finally, in the resource above, you should be familiar (or get familiar) with all of the `git` commands listed.


## Git in the real world

**Let me emphasis again: the most important thing is to have a good understanding of how git works internally; this will help you better understand what you are doing.**

There are also some tricks that you might want to look at:

* Use `git status` whenever you have doubts.
* Create git aliases and tweak the `~/.gitconfig` file to add some default behaviours to your everyday git commands (you can have a look at mine [here](https://gitlab.com/FloChehab/tools-dotfiles/-/blob/master/symlinks_manual/gitconfig) -- I use some aliases there on a daily basis and the config of `[pull]` and `[rebase]` are a game changer).
* `git commit --fixup=` (to correct 'fix' commits -- later)
* `git rebase -i hash^` (the `^` is a game changer, why?)
* Know your way with `HEAD`, `HEAD^`, `HEAD~1`, `HEAD~2`
* `git stash`
* be confident when using `git reset` and `git reset --hard`
* be confident when using `git push -f`
* Don't forget to use the `.gitignore` file (and to set your global `.gitignore` file)
* Know the difference between the branch `origin/main` and `main` (`main` is taken here as an example, it can be any branch)

And the cherry on the cake
* You can have a look at the `git cherry-pick`.


**Finally, when using `git` the most important thing is to make meaningful commits with self-explaining messages: take a look at the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) doc.**


This applies when you are ready for pull / merge request (see the [next subsection](./git-collab.md)) ; don't necessarily pay too much attention on your git history before that ; but make clean PR / MR. Often we should be able to review a PR / MR commit by commit (eg. one commit adds a feature, another one adds the test and finally one some documentation).

To achieve a clean git history, your best friend is the [*interactive rebase*](https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt---interactive).
