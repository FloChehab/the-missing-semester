`git` as collaboration tool
===========================

`git` is the *defacto* code collaboration tool in the industry, and should be for you too.

Collaborating with git is eased with shared repository hosting services such as Github or Gitlab (open source). Those services often offer the same basic core functionalities and other platforms specific behaviours, choices and possibilities.


## *In the real world*

As with any collaborative tool, the most challenging thing in the real world is to set-up conventions and to enforce them.


### Workflows

Each organization often has its own git workflow. A git workflow describes how branching should be achieved, how fixes should be handled, how deployment should be done, etc.

For example, you can check [Gitlab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html).

For small projects, I personally prefer the *Linear git flow* which enables for a clean*ish* git history, etc.



### Peer-review

Github/Gitlab also dramatically ease the process of peer-reviewing each other code. Peer reviews are strictly essential: it's the best way to learn from each other coding skills and to catch typos. Peer reviews also ensure the code base is commonly understood across the team.


### OSS -- Open Source Software

Finally, those hosting services have been clear enablers for Open Source Software, the best thing in the world.


## Do's and don'ts

Well, a lot of the *Do's and don'ts* comes with experience. The most important thing I would say is to do your best not to have multiple people work on the same branch.
