The Missing Semester
====================


Provides a theoretical and technical introduction to some key (full-stack) tech used in the real world. This repo is used as teaching tool.
Inspired by https://missing.csail.mit.edu/ (Licensed under CC BY-NC-SA).