Git exercises
=============

**For each exercises, record all the git commands you have done, we will talk about them later.**

*Yes, I won't give you too many git commands here. You should find your way through those exercises on your own.* Except this one: `git commit --allow-empty -m "An empty commit"` so that you can quickly create commits if necessary.


## Working with a hosted repository

Create a git repository on Gitlab (make sure to set it private so that you don't get shy about what your are doing). Make sure you know how to create commits locally, how to send your commits to the shared repository and how to retrieve the commits / reference from a remote repository. If you'd like, you can play a bit and create issues, protect the `master` branch so that commits can only be added with through a merge-request, etc.

## Cleaning your history.

Let's say you have made many commits just to backup your work along the day. Now, it's time to create a PR and you simply want to put all your commits in one. How do you do it?

**On your own:**
* create a git repository, create many commits on the master branch and find at least two ways to end up with only one commit on master that holds all your work.
* create a git repository, create two commits. How to change the message of the first commit?
* create a git repository, create one commit, create a second commit that fixes the first one (`git commit --fixup=...`). How to merge the first commit and it's fixup in a clean way?

## *Fast-forward only merge*

Search a bit the difference between a simple `git merge` and `git merge --ff-only`. Make drawings if necessary.

**On your own:**
1. Create a git repository. Commit some stuff on a branch `A`, move to another branch `B` (starting from `A`), create other commits there. Move back to branch `A`. Can you merge `B` in `A`? Can you merge `B` in `A` in "fast-forward only" mode? If no, what would you to do first to be able to merge `B` in `A` in "fast-forward only" mode?

Below is an example of what your git history may look like before asking the questions
(`git log --all --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'`).

```
* a557f74 - (B) Second commit on B (2 minutes ago) <Florent Chehab>
* 75e5b86 - First commit on B (2 minutes ago) <Florent Chehab>
* 39cc51d - (HEAD -> A) Second commit on A (2 minutes ago) <Florent Chehab>
* 50583be - First commit on A (2 minutes ago) <Florent Chehab>
* 4170e55 - (master) second commit (2 minutes ago) <Florent Chehab>
* cb9ad50 - first commit (3 minutes ago) <Florent Chehab>
```
*N.b.* Can you tell from the log above, why I know for sure that I am currently on branch `A`?

1. Create a git repository. Commit some stuff on a branch `A`, move to another branch `B` (starting from `A`), create other commits there. Move back to branch `A` and commit new stuff on branch `A`. Can you merge `B` in `A`? Can you merge `B` in `A` in "fast-forward only" mode? If no, what would you to do first to be able to merge `B` in `A` in "fast-forward only" mode?

Below is an example of what your git history may look like before asking the questions:

```
* 2c581b2 - (HEAD -> A) Let's add another commit on A (3 seconds ago) <Florent Chehab>
* 61723f6 - Let's add a commit on A (11 seconds ago) <Florent Chehab>
| * a557f74 - (B) Second commit on B (4 minutes ago) <Florent Chehab>
| * 75e5b86 - First commit on B (4 minutes ago) <Florent Chehab>
|/
* 39cc51d - Second commit on A (5 minutes ago) <Florent Chehab>
* 50583be - First commit on A (5 minutes ago) <Florent Chehab>
* 4170e55 - (master) second commit (5 minutes ago) <Florent Chehab>
* cb9ad50 - first commit (5 minutes ago) <Florent Chehab>
```

## Let's collaborate!

If you don't want things to go wrong, it's usually a good idea not to have two people work on the same branch.

However, it's not always possible to stay with this constraint. So let's go wild and **work in group**.

Create your repo on Gitlab, add your teammates so that they can contribute code and do the following sequentially.

| time | person A              | person B              |
|------|-----------------------|-----------------------|
| 0    | pull, checkout master |                       |
| 1    | commit stuff          |                       |
| 2    | push her work         |                       |
| 3    |                       | pull, checkout master |
| 4    | commit                | commit                |
| 5    | commit                | commit                |
| 6    |                       | push                  |
| 7    | push: kaboum          |                       |

So at step 7, person B work is on the shared repository, and the `git push` from A, should fail.

Find a way to have both A and B work on the repository with the following constraints:

* Do not duplicate commits,
* (bonus) Do not add merge commits
