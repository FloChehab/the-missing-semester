Python exercises
================

## Intro

What is the purpose of having:
```python
if __name__ == "__main__"
```
in a python script file?


Give examples of *pythonic* ways to do things (e.g. list comprehensions, dict comprehensions, generators, etc.)


## Other things to look at

* What is the GIL (global interpreter lock)?
* What does it mean for python to be a garbage collected language?
* What are the advantages of having infinite precision `int`s in python?
* What is duck-typing in python?
