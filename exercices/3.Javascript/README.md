JavaScript exercises
====================

*This section will rely a lot on the https://javascript.info/ tutorial; don't mind looking at other sections of this tutorial that are not linked here (**especially everything in [the JavaScript Fundamentals section](https://javascript.info/first-steps)**).*

## Regarding the language

### Starter questions

[tutorial: types](https://javascript.info/types)

* What are all the primitive types in JavaScript?

[tutorial: objects](https://javascript.info/object)

* What are the possible Object types in JavaScript?

* Which Objects are immutable in Javascript? (by the way, what does it mean for an object to be immutable?)


### Good coding practices

#### `==` vs `===` in Javascript

[tutorial: comparison](https://javascript.info/comparison)

* What is the difference between `==` and `===` in Javascript?

* Why:

```js
"1" == 1 // true
"1" == 0.9999999999 // => false
"1" == 0.9999999999999999999999999 // => true

// ----------------------

"1" === 1 // false
"1" === 0.9999999999 // => false
"1" === 0.9999999999999999999999999 // => false

// -----------------------

const a = {test: 1}, b = {test: 1}; a === b; // => false
```

* Which of `==` or `===` should you always use?


#### `var` vs `let` vs `const`

[tutorial: variables](https://javascript.info/variables) & [tutorial: the old var](https://javascript.info/var)


So what's the general rule?

> Never use `???`, always start with `???`, optionally change to `???` if you need to modify the value!

Replace the `???` by `const`, `var` or `let` accordingly.


#### Spread syntax

[tutorial: Rest parameters and spread syntax](https://javascript.info/rest-parameters-spread)


Run this:

```js
const data = {
  a: "a",
  b: "b",
  c: "c",
  d: "d",
};

const {a, ...others} = data;
```

What is in `a` and `others` variables? Why?


## Tooling

Tooling is very important in JavaScript. For these exercises, we will focus on the transpilers. But first have a look at the *polyfills* section of the tutorial ([tutorial: Polyfills](https://javascript.info/polyfills)).

* Then, let's play a bit with a babel: https://babeljs.io/.

In the `Put in next-gen JavaScript` textarea copy paste the following rather 'modern' JavaScript code:

```js
class Person {

  constructor(name, age){
    this.name = name;
    this.age = age;
  }

  sayHello() {
    const {name, age} = this;
    console.log(`Hello, my name is ${name} and I am ${age} year${age > 1 ? 's' : ''} old.`);
  }
}

const florent = new Person("Florent", 23);
florent.sayHello();
```

In the `Get browser-compatible JavaScript out` does this look familiar? Do you see any class? Do you remember when I was saying that class are just a syntactic sugar?


## Functional Programming in Js

Functions are first-class citizens in JavaScript, and that's pretty great! Here are some highlights.


### A functional API

Check this [tutorial: Array methods](https://javascript.info/array-methods).

Then, look at the exercise below:

```js
const animals = [
    { name: 'Jack', type: 'dog', age: 2 },
    { name: 'Borneo', type: 'cat', age: 14 },
    { name: 'Timi', type: 'dog', age: 4 },
    { name: 'Rick', type: 'dog', age: 14 },
]

const totalDogYears = animals
    . // ...
    . // ...
    . // ...
```

> Compute the variable `totalDogYears` based on `animals`, only by chaining methods such as `map`, `filter` and `reduce` (and arrow functions).


### Support for closure

[tutorial: closure](https://javascript.info/closure#step-4-returning-a-function)


### Higher order function

> Higher order functions are functions that operate on other functions, either by taking them as arguments or by returning them. In simple words, A Higher-Order function is a function that receives a function as an argument or returns the function as output. [(source)](https://blog.bitsrc.io/understanding-higher-order-functions-in-javascript-75461803bad)

* `map` of `filter` methods on `Array`s are HOC.
* This concept is a bit tricky, but just remember the definition and use the freedom offered by HOC support wisely.

## Danger zone: `this` is not what you think

Meditate everything in [tutorial: Object methods, "this"](https://javascript.info/object-methods).
**In particular [this section](https://javascript.info/object-methods#arrow-functions-have-no-this).**

* What are all the types of functions that exist in Javascript?
* Which one is the less error-prone?
* Do you get how useful get be babel with something like [arrow functions as class properties](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties)?


## Playing in your browser

Finally, Js purpose is mainly to interact with the browser / a web page. For this there are multiple APIs.
[tutorial: Modifying the document](https://javascript.info/modifying-document).

Go to [https://www.google.com/](https://www.google.com/), open your browser console, can you change the page background color with JavaScript?


## Async Js

JavaScript speed comes from asynchronous code. If you'd like to know more, check [tutorial: Promises, async/await](https://javascript.info/async).

## Deepen your culture

* What is `V8` and what is `NodeJS`?
* What is `React`?
