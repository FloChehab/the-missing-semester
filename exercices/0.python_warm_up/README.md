Python Warm Up exercice
=======================

Tweak the `count_letters` function in the file [`script.py`](./script.py) so that when you run:

```bash
# Python should be installed on most linux based OS
# (Tested on python >= 3.7)
python3 script.py titiramisu
```

This gets printed:

```
i: 3
t: 2
r: 1
a: 1
m: 1
s: 1
u: 1
```

*NB. When the character counts are the same, the print order is not important.*


**Do it in the most straightforward / understandable way you can :smile:.**
