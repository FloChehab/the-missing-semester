import sys
from typing import Tuple, Iterable


def count_letters(string: str) -> Iterable[Tuple[str, int]]:
    """
    Method to count the letters in a string and return the result as an ordered iterable.

    :param string: Input string
    :return: An iterable where each item is a tuple (char, count), where char is character
    from the input string and count in its occurrence. Must be ordered by count desc.
    """
    return []


if __name__ == '__main__':
    input_string = sys.argv[1]
    res = count_letters(input_string)
    for char, count in res:
        print(f"{char}: {count}")
